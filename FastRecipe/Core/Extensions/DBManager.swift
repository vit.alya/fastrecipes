//
//  DBManager.swift
//  FastRecipe
//
//  Created by Vity Kinchin on 18.11.2022.
//

import Foundation
import RealmSwift

protocol DBManager {
    
    func save(model: RecipeModel)
    func obtainModels() -> [RecipeModel]
}

class DBManagerImpl: DBManager {
    
    fileprivate lazy var mainRealm = try! Realm(configuration: .defaultConfiguration)
    
    func save(model: RecipeModel){
        try! mainRealm.write {
            mainRealm.add(model)
        }
    }
    
    func obtainModels() -> [RecipeModel] {
        let models = mainRealm.objects(RecipeModel.self)
        
        return Array(models)
    }
}

