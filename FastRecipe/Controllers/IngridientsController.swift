//
//  IngridientsController.swift
//  FastRecipe
//
//  Created by Vity Kinchin on 16.11.2022.
//

import UIKit

class IngridientsController: UIViewController {
    
    var tableView = UITableView()

    public var reciepes: GetRecipesResponse? 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        view.backgroundColor = Resources.Colors.customBlue
        configureGoBackButton()
        
    }
    
    func configureTableView () {
        view.addSubview(tableView)
        setTableViewDelegates()
        tableView.rowHeight = 100
        tableView.pin(to: view)
        view.backgroundColor = Resources.Colors.customBlue
        tableView.register(IngridientsCell.self, forCellReuseIdentifier: "IngridientsCell")
    }
    
    func setTableViewDelegates() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    fileprivate let goBackButton: UIButton = {
        let button = UIButton(type: .system)
        //первая часть кнопки
        let attributedTitle = NSMutableAttributedString(string: "Back", attributes: [.font: UIFont.systemFont(ofSize: 18), .foregroundColor: Resources.Colors.customOrange])
        //вторая часть кнопки
        attributedTitle.append(NSAttributedString(string: " ", attributes: [.font: UIFont.systemFont(ofSize: 18), .foregroundColor: Resources.Colors.customOrange]))
        button.setAttributedTitle(attributedTitle, for: .normal)
        // переход
        button.addTarget(self, action: #selector(goBack), for: .touchUpInside)
        button.backgroundColor = Resources.Colors.customBlue
        button.layer.cornerRadius = 22
        return button
    } ()
    
    @objc fileprivate func goBack() {
        dismiss(animated: true, completion: nil)
    }
    
    private func configureGoBackButton () {
        view.addSubview(goBackButton)
        goBackButton.anchor(top: nil, leading: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.trailingAnchor, padding: .init(top: 0, left: 40, bottom: 10, right: 40))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    fileprivate func setupTapGesture() {
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapDismiss)))
    }
    
    @objc fileprivate func handleTapDismiss() {
        view.endEditing(true)
    }
    
}

extension IngridientsController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reciepes?.recipes.first?.extendedIngredients.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "IngridientsCell", for: indexPath) as? IngridientsCell else {
            return UITableViewCell()
        }
        cell.setContent(title: reciepes?.recipes.first?.extendedIngredients[indexPath.row].name ?? "don't have title",
                        iconName: reciepes?.recipes.first?.extendedIngredients[indexPath.row].image ?? "raw-bacon.png")
        return cell
    }
}



