//
//  ListViewController.swift
//  FastRecipe
//
//  Created by Vity Kinchin on 14.11.2022.
//

import UIKit

class ListViewController: UIViewController  {
    
    let tableView : UITableView = {
        let t = UITableView()
        t.translatesAutoresizingMaskIntoConstraints = false

        return t
    }()
    let identifire = "jopa"
    let dbManager: DBManager = DBManagerImpl()
    var recipeModels = [RecipeModel()]

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Resources.Colors.customBlue
        tableView.rowHeight = 110
        tableView.frame = CGRect(x: 0, y: 100, width: 390, height: 600)
        tableView.backgroundColor = Resources.Colors.customBlue
        tableView.register(LikesCell.self, forCellReuseIdentifier: identifire)
        tableView.delegate = self
        tableView.dataSource = self
        self.view.addSubview(tableView)
        getRecipeModels()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getRecipeModels()
    }
    
    private func getRecipeModels() {
        recipeModels = dbManager.obtainModels()
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            tableView.beginUpdates()
            recipeModels.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .none)
            tableView.endUpdates()
        }
    }
}

extension ListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        recipeModels.count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifire , for: indexPath) as? LikesCell else {
            return UITableViewCell()
        }
        cell.setContent(title: recipeModels[indexPath.row].title, iconName: recipeModels[indexPath.row].image)
        return cell
    }
}

    



