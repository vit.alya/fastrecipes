//
//  TabBarController.swift
//  FastRecipe
//
//  Created by Vity Kinchin on 11.11.2022.
//

import UIKit

enum Tabs: Int {
    
    case main
    case list
}

final class TabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    private func configure () {
        tabBar.backgroundColor = Resources.Colors.customOrange
        
        tabBar.layer.borderColor = Resources.Colors.customOrange.cgColor
        tabBar.layer.borderWidth = 0
        tabBar.layer.masksToBounds = true
        
        let mainNavigation = UINavigationController(rootViewController: MainViewController() )
        mainNavigation.tabBarItem.image = UIImage(named: "barMainUnselected")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        mainNavigation.tabBarItem.selectedImage = UIImage(named: "barMainSelected")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        
        let listNavigation = UINavigationController(rootViewController: ListViewController() )
        listNavigation.tabBarItem.image = UIImage(named: "barListUnselected")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        listNavigation.tabBarItem.selectedImage = UIImage(named: "barListSelected")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        
        setViewControllers([mainNavigation, listNavigation], animated: false)
    }
}
