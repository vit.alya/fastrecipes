//
//  MainViewController.swift
//  FastRecipe
//
//  Created by Vity Kinchin on 14.11.2022.
//

import UIKit
import RealmSwift

class MainViewController: UIViewController {
    
    let dbManager: DBManager = DBManagerImpl()
    
    private let topView: UIView = {
        let view = UIView()
        view.backgroundColor = Resources.Colors.customBlue
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private let botView: UIView = {
        let view = UIView()
        view.backgroundColor = Resources.Colors.customOrange
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let receipeImage: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
        let recipeTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        //label.font = UIFont(name: "Marhey-VariableFont_wght")
        label.font = UIFont.systemFont(ofSize: 30.0)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textColor = Resources.Colors.customOrange
        label.backgroundColor = Resources.Colors.customBlue
        return label
    }()
    
    private let IngridientsButton: UIButton = {
        let button = UIButton(type: .system)
        //первая часть кнопки
        let attributedTitle = NSMutableAttributedString(string: "Ingridients.", attributes: [.font: UIFont.systemFont(ofSize: 18), .foregroundColor: Resources.Colors.customOrange])
        //вторая часть кнопки
        attributedTitle.append(NSAttributedString(string: " Show", attributes: [.font: UIFont.systemFont(ofSize: 18), .foregroundColor: Resources.Colors.customOrange ]))
        button.setAttributedTitle(attributedTitle, for: .normal)
        // переход
        button.addTarget(self, action: #selector(showIngridients), for: .touchUpInside)
        button.backgroundColor = Resources.Colors.customBlue
        button.layer.cornerRadius = 22
        return button
    } ()
    
    private let likeButton: UIButton = {
        let button = UIButton(type: .system)
        //первая часть кнопки
        let attributedTitle = NSMutableAttributedString(string: "Like", attributes: [.font: UIFont.systemFont(ofSize: 18), .foregroundColor: Resources.Colors.customOrange])
        //вторая часть кнопки
        attributedTitle.append(NSAttributedString(string: " ", attributes: [.font: UIFont.systemFont(ofSize: 18), .foregroundColor: Resources.Colors.customOrange ]))
        button.setAttributedTitle(attributedTitle, for: .normal)
        // переход
        button.addTarget(self, action: #selector(addRecipeInModel), for: .touchDown)
        button.backgroundColor = Resources.Colors.customBlue
        button.layer.cornerRadius = 22
        return button
    } ()
    
    private var recipes: GetRecipesResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
        configureTopView()
        configureBotView()
        configureRecipeImage()
        configureLikeButton()
        configureIngridientsButton()
        view.addSubview(recipeTitle)
        takeAPI()
        
        receipeImage.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        view.addGestureRecognizer(tap)
        
        NSLayoutConstraint.activate([
            recipeTitle.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            recipeTitle.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            recipeTitle.bottomAnchor.constraint(equalTo: receipeImage.topAnchor, constant: 16)
        ])
        recipeTitle.textAlignment = .center
        
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
            takeAPI()
    }
    
    private func takeAPI () {
        guard let url = URL(string: "https://api.spoonacular.com/recipes/random?apiKey=4221d9b1898942599be2546b5b3e56d1") else { return }
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                print(error)
                return
            }
            guard let data = data else { return }
            
            do {
                let reciepes = try JSONDecoder().decode(GetRecipesResponse.self, from: data)
                self.recipes = reciepes
                self.giveImage()
                self.giveTitle()
            }catch{
                print(error)
            }
        }.resume()
    }
    
    private func configureTopView() {
        view.addSubview(topView)
        NSLayoutConstraint.activate([
            topView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            topView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0),
            topView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0),
            topView.heightAnchor.constraint(equalToConstant: view.frame.size.height/2)
        ])
    }
    
    private func configureBotView() {
        view.addSubview(botView)
        NSLayoutConstraint.activate([
            botView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            botView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0),
            botView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0),
            botView.heightAnchor.constraint(equalToConstant: view.frame.size.height/2)
        ])
    }
   
    private func configureRecipeImage() {
        view.addSubview(receipeImage)
        receipeImage.layer.cornerRadius = 10
        NSLayoutConstraint.activate([
            receipeImage.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            receipeImage.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            receipeImage.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16),
            receipeImage.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16),
            ])
        receipeImage.contentMode = .scaleAspectFit
        receipeImage.layer.cornerRadius = receipeImage.frame.size.width / 2
        receipeImage.clipsToBounds = true
        receipeImage.image = receipeImage.image?.withRenderingMode(.alwaysTemplate)
        receipeImage.tintColor = UIColor.red    }
    
    private func configureIngridientsButton() {
        view.addSubview(IngridientsButton)
        IngridientsButton.anchor(top: nil, leading: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.trailingAnchor, padding: .init(top: 0, left: 40, bottom: 10, right: 40))
    }
    
    private func configureLikeButton() {
        view.addSubview(likeButton)
        likeButton.anchor(top: receipeImage.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 40, left: 20, bottom: 0, right: 20))        
    }
    
    private func giveImage()  {
        let url = URL(string: recipes?.recipes.first!.image ?? "")
        recipes?.recipes.forEach({ recipe in
            print(recipe.image)
        })
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: url!)
            DispatchQueue.main.async { [self] in
                self.receipeImage.image = UIImage(data: data!)
                
            }
        }
    }
    
    private func giveTitle()  {
        DispatchQueue.main.async {

            self.recipeTitle.text = self.recipes?.recipes.first?.title
        }
    }
    
    @objc fileprivate func showIngridients() {
        let ingridientsController = IngridientsController()
        ingridientsController.reciepes = recipes
        present(ingridientsController, animated: true, completion: nil)
    }
    
    @objc func addRecipeInModel() {
        let model = RecipeModel()
        let realm = try! Realm()
        try! realm.write{
            model.image = recipes?.recipes.first!.image ?? ""
            model.title = recipes?.recipes.first!.title ?? ""
        }
        dbManager.save(model: model)
    }
}


