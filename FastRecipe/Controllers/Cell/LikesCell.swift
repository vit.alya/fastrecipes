//
//  LikesCell.swift
//  FastRecipe
//
//  Created by Vity Kinchin on 16.11.2022.
//

import UIKit

class LikesCell: UITableViewCell {

    var likesImageView = UIImageView()
    var likesTitleLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super .init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(likesImageView)
        addSubview(likesTitleLabel)
        configureImageView()
        configureTitleLabel()
        setImageConstraints()
        setTitleLabelConstraint()
        backgroundColor = Resources.Colors.customOrange
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureImageView() {
        likesImageView.layer.cornerRadius = 10
        likesImageView.clipsToBounds = true
    }
    
    private func configureTitleLabel() {
        likesTitleLabel.numberOfLines = 0
        likesTitleLabel.adjustsFontSizeToFitWidth = true
        likesTitleLabel.textColor = Resources.Colors.customBlue
    }
    
    private func setImageConstraints() {
        likesImageView.translatesAutoresizingMaskIntoConstraints = false
        likesImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        likesImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12).isActive = true
        likesImageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        likesImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
    }
    
    private func setTitleLabelConstraint() {
        likesTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        likesTitleLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        likesTitleLabel.leadingAnchor.constraint(equalTo: likesImageView.trailingAnchor, constant: 20).isActive = true
        likesTitleLabel.heightAnchor.constraint(equalToConstant: 80).isActive = true
        likesTitleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12).isActive = true
    }
    
    public func setContent(title: String, iconName: String) {
        likesTitleLabel.text = title
        guard let iconUrl = URL(string: iconName) else {
            return
        }
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: iconUrl)
            DispatchQueue.main.async { [self] in
                self.likesImageView.image = UIImage(data: data!)
                
            }
        }
    }
}
