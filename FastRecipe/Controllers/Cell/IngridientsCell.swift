//
//  IngridientsCell.swift
//  FastRecipe
//
//  Created by Vity Kinchin on 16.11.2022.
//

import UIKit

class IngridientsCell: UITableViewCell {
    
    var ingridientImageView = UIImageView()
    var ingridientTitleLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super .init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(ingridientImageView)
        addSubview(ingridientTitleLabel)
        configureImageView()
        configureTitleLabel()
        setImageConstraints()
        setTitleLabelConstraint()
        backgroundColor = Resources.Colors.customOrange
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureImageView() {
        ingridientImageView.layer.cornerRadius = 30
        ingridientImageView.clipsToBounds = true
    }
    
    private func configureTitleLabel() {
        ingridientTitleLabel.numberOfLines = 0
        ingridientTitleLabel.adjustsFontSizeToFitWidth = true
        ingridientTitleLabel.textColor = Resources.Colors.customBlue
    }
    
    func setImageConstraints() {
        ingridientImageView.translatesAutoresizingMaskIntoConstraints = false
        ingridientImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        ingridientImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12).isActive = true
        ingridientImageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        ingridientImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
    }
    
    func setTitleLabelConstraint() {
        ingridientTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        ingridientTitleLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        ingridientTitleLabel.leadingAnchor.constraint(equalTo: ingridientImageView.trailingAnchor, constant: 20).isActive = true
        ingridientTitleLabel.heightAnchor.constraint(equalToConstant: 80).isActive = true
        ingridientTitleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12).isActive = true
    }
    
    public func setContent(title: String, iconName: String) {
        ingridientTitleLabel.text = title
        guard let iconUrl = URL(string: "https://spoonacular.com/cdn/ingredients_100x100/" + iconName) else {
            return
        }
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: iconUrl)
            DispatchQueue.main.async { [self] in
                self.ingridientImageView.image = UIImage(data: data!)
                
            }
        }
    }
}
