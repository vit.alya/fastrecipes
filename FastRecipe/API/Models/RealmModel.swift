//
//  RealmModel.swift
//  FastRecipe
//
//  Created by Vity Kinchin on 18.11.2022.
//

import Foundation
import RealmSwift

@objcMembers
public class RecipeModel: Object {
    dynamic var uniqId = UUID().uuidString
    dynamic var title = String()
    dynamic var image = String()
}

