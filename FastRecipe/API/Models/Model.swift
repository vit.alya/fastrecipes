//
//  Model.swift
//  FastRecipe
//
//  Created by Vity Kinchin on 14.11.2022.
//

import Foundation

struct GetRecipesResponse: Decodable {
    let recipes: [Recipe]
}
struct Recipe: Decodable {
    let vegetarian: Bool
    let vegan: Bool
    let glutenFree: Bool
    let dairyFree: Bool
    let veryHealthy: Bool
    let cheap: Bool
    let veryPopular: Bool
    let sustainable: Bool
    let lowFodmap: Bool
    let weightWatcherSmartPoints: Int
    let gaps: String
    let preparationMinutes: Int
    let cookingMinutes: Int
    let aggregateLikes: Int
    let healthScore: Int
    let creditsText: String
    let sourceName: String
    let pricePerServing: Double
    let extendedIngredients: [ExtendedIngredients]
    let id: Int
    let title: String
    let readyInMinutes: Int
    let servings: Int
    let sourceUrl: String
    let image: String
    let imageType: String
    let summary: String
    let dishTypes: [String]
    let diets: [String]
    let instructions: String
    let spoonacularSourceUrl: String
    
}
struct ExtendedIngredients: Decodable {
    let id: Int
    let aisle: String
    let image: String
    let consistency: String
    let name: String
    let nameClean: String
    let original: String
    let originalName: String
    let amount: Double
    let unit: String
}

struct Measures: Decodable {
    let us: [Us]
    let metric: [Metric]
}
struct Us: Decodable {
    let amount: Double
    let unitShort: String
    let unitLong: String
}
struct Metric: Decodable {
    let amount: Double
    let unitShort: String
    let unitLong: String
}

struct AnalyzedInstructions: Decodable  {
    let name: String?
    let steps: [Steps]
}
struct Steps: Decodable {
    let number: Int
    let step: String
    let ingridients: [Ingridients]
    let equipment: [Equipment]
}
struct Ingridients: Decodable {
    let id: Int
    let name: String
    let localizedName: String
    let image: String
}
struct Equipment: Decodable {
    let id: Int
    let name: String
    let localizedName: String
    let image: String
}
